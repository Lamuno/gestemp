

import java.io.IOException;
import java.io.PrintWriter;

import java.sql.*;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/gi")
public class ingenierosServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public ingenierosServlet() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		HTML5.comienzo(out, "Gestión de Ingenieros");
		out.println("<div style='width: 300px; margin: auto; padding: 25px 25px 25px 25px; background-color: white; border: 1px solid rgb(0,0,0,0.25); border-radius: 10px;'>");
		out.println("<form method=\"get\" action=\"/gi\">");
		out.println("<input placeholder='NSS' type='number' required></input><br/>");
		out.println("<input placeholder='Nombre' type='text' required></input><br/>");
		out.println("<input placeholder='Salario' type='number' required></input><br/>");
		out.println("Departamento: <select name=\"dep\">");
		agregarDepartamentos(out);
		out.println("</select><br/>");
		out.println("<button type='submit'>Dar de Alta</button>");
		out.println("</form>");
		out.println("</div>");
		HTML5.fin(out);
	}

	private void agregarDepartamentos(PrintWriter out) {
		try {
			Connection c = DriverManager.getConnection("jdbc:mysql://localhost:3306/Gestión de Empleados",
					"davidrr", "practicas");
			Statement sql = c.createStatement();
			ResultSet rs = sql.executeQuery("SELECT nombre FROM departamentos");
			while (rs.next())
				out.printf("<option>%s</option>\n", rs.getString("nombre"));
			rs.close();
			c.close();
		}catch (SQLException e) {
			
		}
	}

}
