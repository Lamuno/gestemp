import java.io.PrintWriter;

public class HTML5 {
	
	static void comienzo(PrintWriter out, String title) {
		out.println("<!DOCTYPE html>");
		out.println("<html>");
		out.println("<head>");
		out.println("<meta charset=\"UTF-8\">");
		out.println("<title>" + title + "</title>");
		out.println("<style>"
				+ "*{"
				+ "text-align: center;"
				+ "} "
				+ "</style>");
		out.println("</head>");
		out.println("<body style='background-color: steelblue;'>");
	}
	
	static void fin(PrintWriter out) {
		out.println("</body>");
		out.println("</html>");
	}
}
